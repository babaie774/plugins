jQuery(document).ready(function ($) {

    $(document).on('click', '#save_settings', function ($event) {
        $event.preventDefault();
        var loader = $('#loading');
        var settings_value = $('#some_settings').val();
        loader.slideDown(1000);
        $.ajax({
            url: subscribe_data.ajax_url,
            type: 'post',
            dataType: 'json',
            data: {
                action: 'save_settings',
                setting_value: settings_value,
                nonce : subscribe_data.nonce
            },
            success: function (response) {
                if (response.success) {
                    alert(response.message);
                } else {
                    alert(response.message);
                }
                loader.slideUp(1000);
            },
            error: function () {

            }
        });
    });
    $(document).on('click','#slider_handler',function (evt) {
        evt.preventDefault();
        var custom_uploader;
        var $this = $(this);
        var $target = $this.data('target');
        var $target_type = $this.data('target-type');
        if (custom_uploader) {
            custom_uploader.open();
            return;
        }
        custom_uploader = wp.media.frames.file_frame = wp.media({
            title: 'انتخاب تصویر',
            button: {
                text: 'انتخاب تصویر'
            },
            multiple: false
        });
        custom_uploader.on('select', function () {
            attachment = custom_uploader.state().get('selection').first().toJSON();
            // $this.prev().val(attachment.url);
            $('#slider_image').val(attachment.url);
            $this.prev().attr('src',attachment.url);
        });
        custom_uploader.open();

    });
});